# BPTK-Py Tutorial

Welcome to the BPTK-Py tutorial. This tutorial contain sample agent-based and System Dynamics models created using the BPTK-Py framework. The tutorial is designed as a companion to the [BPTK_Py online documentation](http://bptk.transentis-labs.com).

The objective of the tutorials is to illustrate how to use the BPTK-Py framework, not to introduce System Dynamics modeling or Agent-based modeling in general.

We illustrate the framework using a simple project management model and the Bass Diffusion model:

* __Simple Project Management Model__. This model is an illustration of [Parkinson's law](https://en.wikipedia.org/wiki/Parkinson%27s_law), which states that work expands so as to fill the time available for its completion.
* __Bass Diffusion Model__. This model is an implementation of the [Bass diffusion model](https://en.wikipedia.org/wiki/Bass_diffusion_model) originally created by [Frank Bass](https://en.wikipedia.org/wiki/Frank_Bass) that describes the process of how new products get adopted in a population. The Bass model has been widely used in forecasting, especially new products' sales forecasting and technology forecasting.

You can learn more about these models on our blog:

* [Step-by-step introduction to System Dynamics](https://www.transentis.com/step-by-step-tutorials/introduction-to-system-dynamics/) using the simple project management model.
* Introduction to the [Bass Diffusion Model](https://www.transentis.com/causal-loop-diagramming/).

Here is an overview of the documents contained in this tutorial:

* [Writing Computational Essays Using Simulation Models](bptk_py_introduction.ipynb). Introduction to using simulation models built with ®Stella in Jupyter Notebooks using the BPTK-Py framework.
* [A Simple Python Library for System Dynamics](bptk_py_sd_dsl_intro.ipynb). Introduction to using the BPTK-Py framework to built SD models directly in Juptyer Notebooks.
* [Introduction to Agent Based Simulation Modeling with BPTK-Py](bptk_py_abm_intro.ipynb). This notebook illustrates how to create an agent-based implementation of a simple project management model.
* [An Agent-based Implementation of the Bass Diffusion Model](bptk_py_bass_diffusion.ipynb). A simple notebook to run an agent-based implementation of the Bass Diffusion model.
* [In Depth Discussion Of The Business Prototyping Toolkit for Python](bptk_py_in_depth.ipynb) A discussion of how the BPTK-Py framework works and some of the advanced API features not covered in the other notebooks.
* [The Architecture of the BPTK-Py Framework](bptk_py_architecture.ipynb) Explains the overall architecture of the BPTK-Py framework.
* [How To: Creating User Defined Functions in SD Models](how_to_sd_user_defined_functions.ipynb) Explains how to create user defined functions in SD models.
* [How To: Developing Advanced User-Interfaces](how_to_developing_advanced_user_interfaces.ipynb) Explains how to develop more advnaces user interfaces using Jupyter Widgets, Pandas dataframes and Matplotlib.

